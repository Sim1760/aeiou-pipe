# aeiou-pipe

# pulseaudio works with paplay
# dependencies: requests, pulseaudio, os
import sys
import requests
import os
from urllib.request import urlretrieve
import urllib.parse
import time
import wave

infiles = []
outfile = "output.wav"

# chunking function
def get_chunks(s, maxlength):
  start = 0
  end = 0
  while start + maxlength < len(s) and end != -1:
    end = s.rfind(" ", start, start + maxlength + 1)
    yield s[start:end]
    start = end +1
  yield s[start:]

# accept text from pipe
text = sys.stdin.read()

# chunk into 1000 or less
chunks = get_chunks(text, 1000)
url = "https://tts.cyzon.us/tts?text="

# break into 1000-character sequences
chunknum = 0

# print length of time, chunk num, get file output name
#request_count = sum(1 for _ in chunks) # chunks is of type generator w/ no len() member
#time = (6 * request_count) / 60 # sec to min

#print("Job will take " + str(time) + "minutes to complete.") # Proceed? [y/n])
#response = input()
#if response == 'n' or 'N':
#    exit()

# loop over the sequences
for chunk in chunks:
  # encode
  chunk = urllib.parse.quote(chunk)
  payload = url+chunk
  name = "chunk" + str(chunknum) + ".wav"
  filename = open(name, "wb")
  
  # send request to aeiou
  try:
    r = requests.get(payload, timeout=5)
    print(r.status_code)
    # elif tree here
    if r.status_code == 400:
        print("400 bad request.\n Input string is invalid or empty. Do not retry.\nAborting...")
        exit()
    elif r.status_code == 413:
        print("413 payload too large.\n String submitted exceeds maximum length per request (1024 characters). Do not retry.\nAborting...")
        exit()
    elif r.status_code == 429:
        print("429 too many requests. Rate limit exceeded, soft cap 15/1min, hard cap (ban) 30/1min.\nAborting...")
        exit()
    elif r.status_code == 500:
        print("500 internal server error. Some inputs can crash DECTalk.\nAborting...")
        exit()
    elif r.status_code == 503:
        print("503 service unavailable. Temporary internal failure. Wait a bit before retrying.\nAborting...")
        exit()
    elif r.status_code != 200:
        print("Unknown status code " + str(r.status_code) + ".\nAborting...")
        exit()
    filename.write(r.content)
  except:
    print("Problem with API request not covered by status codes.\nAborting...")
    exit()
    
  statement = "writing " + name
  print(statement)
  filename.close()
  infiles.append(name)
  chunknum += 1
  # wait 6 seconds between requests to prevent going over 14/req per 1min
  time.sleep(6)

# patch the chunks together into a bigger wav
data = []
for file in infiles:
  w = wave.open(file, "rb")
  data.append([w.getparams(), w.readframes(w.getnframes())])
  w.close()
  command = "rm " + file
  os.system(command)
output = wave.open(outfile, "wb")
output.setparams(data[0][0])
for i in range(len(data)):
    output.writeframes(data[i][1])
output.close()

# play it
#os.system("paplay output.wav")
