# aeiou pipe

## Description

This project seeks to make a utility like eSpeak in that it accepts piped outputs from the command line and sends them 
direct to a TTS engine. For better voice clarity, I want to use DecTalk through calzoneman's aeiou API. This script accepts US-ASCII text piped in from stdin, creates metered requests to the remote aeiou API, chunks the returned .wav outputs, and ultimately stitches them together into one output.wav file of the text input.

https://github.com/calzoneman/aeiou/blob/master/docs/usage-guidelines.md

https://tts.cyzon.us/

https://github.com/calzoneman/aeiou

## Dependencies
I assume a Linux environment with bash terminal.
- Common Linux utilities are intended to be used (cat, paplay (pulseaudio), etc).
- Python3 with requests package is required as well. All others should be standard imports.

## Usage
For now, something like 'cat body_of_text.txt | python3 aeiou_pipe.py' for larger bodies of text.

For testing, try 'echo "Whatever fun thing you'd like to say!" | python3 aeiou_pipe.py && paplay output.wav'

## Notes
This is a work in progress and mostly just intended for my personal use.
- Transmission rates are intentionally limited such that they do not exceed the rules of the aeiou API. Do not change them or the API will block you.
- Text parsing prior to this is entirely up to you. aeiou only handles US-ASCII and Unicode submissions will either generate strange outputs or fail to return. 
- If you hear something like 'command error in phoneme' in the .wav output playback, this is indicative of a DECtalk error. This could be from uninterpretable inputs (Unicode) or from partial DECtalk commands that were not intended to be there. The aeiou API runs DECtalk 4.61, but below is a copy of the DECtalk manual for 5.01 for some relevant insight on commands.
  - https://www.digikey.com/htmldatasheets/production/1122220/0/0/1/DECtalk-Guide.pdf
